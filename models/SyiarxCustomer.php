<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log".
 *
 * @property int $id
 * @property string $created_dt
 * @property string $created_tm
 * @property string $func
 * @property string $req_xml
 * @property string $req_json
 * @property string $resp_xml
 * @property string $resp_json
 * @property int $application_no
 */
class SyiarxCustomer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'syiarex_customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'legal_id_no'], 'safe'],
    
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'legal_id_no' => 'Created Dt',
          
        ];
    }


    public static function getListToday(){
        $data=SyiarxCustomer::find(['id','legal_id_no'])->where(['local_created_date'=>date('Y-m-d'),'cust_type'=>'R','legal_type'=>'01-KTP'])->andWhere("not (legal_id_no ~* '[^0-9]') and not (id ~* '[^0-9]') 
and length(legal_id_no)=16 and length(id)=10")->all();
    }

    public static function saveLog($data){
        date_default_timezone_set("Asia/Jakarta");
        $log=new Log();
        $log->application_numbers=$data['application_no'];
        $log->func=$data['func'];
        $log->type=$data['type'];
        $log->req_xml=$data['req_xml'];
        $log->req_json=$data['req_json'];
        $log->resp_xml=$data['resp_xml'];
        $log->resp_json=$data['resp_json'];
        $log->created_dt=date("Y-m-d");
        $log->created_tm=date("H:i:s");
        $log->channel=$data['channel'];
        $log->save();
    }
}
