<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\SyiarxCustomer;
use app\models\User;
use yii\httpclient\Client;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;
/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DatabalikanController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }
    
    public function actionSyiarex() {
        
        $tgl=date('Y-m-d');
//        $tgl='2020-07-22';
//        var_dump($tgl);die;
        
        echo "======================================\r\n";
        echo "DATA BALIKAN DUKCAPIL HASIL : SYIAREX\r\n";
        echo "======================================\r\n";
        
        $list = SyiarxCustomer::find()->select(['legal_id_no', 'id'])->where(['local_created_date' => $tgl, 'legal_type' => '01-KTP'])
                ->andWhere("not (legal_id_no ~* '[^0-9]') and not (id ~* '[^0-9]') 
                            and length(legal_id_no)=16 and length(id)=10 and(cust_type='R' or local_customer_type='INDIVIDU')")
                ->all();
        echo "Jumlah Data : ".count($list)."\r\n";
        foreach ($list as $data) {
            echo $data['id'].'-'.$data['legal_id_no'];
            $request = new Client();
            $response = $request->createRequest()
                ->setMethod('POST')
                ->setUrl('http://172.16.160.31/databalikan/api/store')
                ->addHeaders(['authorization' => "Bearer kC5gqPOOWaZSJKmKIBl1imki1DBN4quJKotVgjO8TVy5fcKX2wN7EZ0vbHB5IqMorVv2YTrhJhG2mk0r4Zz7mMSHX0GW3JI8U5ji"])
                ->setData(['nik' => $data['legal_id_no'],
                    'id_lembaga' => '080077',
                    'nama_lembaga' => 'BRI_SYARIAH',
                    'param' => '[{"CIF_NO":"'.$data['id'].'"}]',
                ])
                ->send();
            if ($response->isOk) {
                echo($response->getContent());
            }
            echo "\r\n";
        }
        
        echo "\r\n";
        echo "\r\n";
        echo "DONE";
        echo "\r\n";
        echo "\r\n";
        echo "\r\n";

//
       
    }

}
