<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\SyiarxCustomer;
use app\models\User;
use yii\httpclient\Client;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBasicAuth;

class DatabalikanController extends \yii\rest\Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {

        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
  

    public function actionSyiarex() {
        
        $tgl=date('Y-m-d');
        $tgl='2020-07-21';
//        var_dump($tgl);die;
        $list = SyiarxCustomer::find()->select(['legal_id_no', 'id'])->where(['local_created_date' => $tgl, 'legal_type' => '01-KTP'])
                ->andWhere("not (legal_id_no ~* '[^0-9]') and not (id ~* '[^0-9]') 
                            and length(legal_id_no)=16 and length(id)=10 and(cust_type='R' or local_customer_type='INDIVIDU')")
                ->all();
        foreach ($list as $data) {
            echo $data['id'];
            $request = new Client();
            $response = $request->createRequest()
                ->setMethod('POST')
                ->setUrl('http://172.16.160.31/databalikan/api/store')
                ->addHeaders(['authorization' => "Bearer kC5gqPOOWaZSJKmKIBl1imki1DBN4quJKotVgjO8TVy5fcKX2wN7EZ0vbHB5IqMorVv2YTrhJhG2mk0r4Zz7mMSHX0GW3JI8U5ji"])
                ->setData(['nik' => $data['legal_id_no'],
                    'id_lembaga' => '080077',
                    'nama_lembaga' => 'BRI_SYARIAH',
                    'param' => '[{"CIF_NO":"'.$data['id'].'"}]',
                ])
                ->send();
            if ($response->isOk) {
                echo($response->getContent()."<br/>");
            }
            echo "<br/>";
        }
        
        die;
//
        $request = new Client();
        $response = $request->createRequest()
                ->setMethod('POST')
                ->setUrl('http://172.16.160.31/databalikan/api/store')
                ->addHeaders(['authorization' => "Bearer kC5gqPOOWaZSJKmKIBl1imki1DBN4quJKotVgjO8TVy5fcKX2wN7EZ0vbHB5IqMorVv2YTrhJhG2mk0r4Zz7mMSHX0GW3JI8U5ji"])
                ->setData(['nik' => '3302241205930003',
                    'id_lembaga' => '080077',
                    'nama_lembaga' => 'BRI_SYARIAH',
                    'param' => '[{"CIF_NO":"Test"}]',
                ])
                ->send();
        var_dump($response);
        if ($response->isOk) {
            var_dump($response->getContent());
        }

        die;
    }

}
